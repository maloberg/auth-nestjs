import { Injectable } from "@nestjs/common";

@Injectable()
export class AppService {
    getHello(): string {
        this.fizzBuzz(40);
        return "Hello world bro";
    }

    fizzBuzz(range: number = 10) {
        let string = "";
        for (let index = 0; index < range; index++) {
            if (index % 3 === 0) {
                string += "Fizz";
            }

            if (index % 3 === 0) {
                string += "Buzz";
            }

            if (string === "") {
                string += String(index);
            }

            console.log(string);
            string = "";
        }
    }

}

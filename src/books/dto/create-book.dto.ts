import { IsOptional, IsString, MaxLength, MinLength } from "class-validator";

export class CreateBookDto {
    @IsString()
    title: string;

    @IsString()
    description: string;

    @IsOptional()
    @IsString()
    @MaxLength(10)
    @MinLength(10)
    isbn: string;
}

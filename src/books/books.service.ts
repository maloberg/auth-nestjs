import { Injectable } from '@nestjs/common';
import { PrismaService } from "src/prisma/prisma.service";
import { CreateBookDto } from './dto/create-book.dto';
import { UpdateBookDto } from './dto/update-book.dto';

@Injectable()
export class BooksService {
  constructor(private prisma: PrismaService) {}

  async create(createBookDto: CreateBookDto) {
    try {
      const book =  await this.prisma.book.create({data: createBookDto});
      return book;
    } catch(error) {
      console.log(`error`, error)
    }
  }

  findAll() {
    return this.prisma.book.findMany();
  }

  findOne(id: string) {
    return this.prisma.book.findUnique({ where: { id }});
  }

  async update(id: string, updateBookDto: UpdateBookDto) {
    const book = await this.prisma.book.update({where: { id }, data: updateBookDto});
  }

  async remove(id: string) {
    try {
      await this.prisma.book.delete({where: { id }})
    } catch(err) {
      // Need some error handling here but i'm lazy for now
    }
    return { success: true }
  }
}

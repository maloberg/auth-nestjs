export type Tokens = {
    access_token: string;
    refresh_token: string;
};

export type TokensPayload = {
    userId: string;
    email: string;
};

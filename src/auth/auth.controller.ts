import {
    Body,
    Controller,
    HttpCode,
    HttpStatus,
    Post,
    Res,
    UseGuards,
} from "@nestjs/common";

import { AuthDto } from "./dto";
import { Tokens } from "./types";
import { RtGuard } from "src/common/guard";

import {
    GetCurrentUserId,
    Public,
    GetCurrentUser,
} from "src/common/decorators";

import { Response } from "express";
import { AuthService } from "./auth.service";

@Controller("auth")
export class AuthController {
    constructor(private authService: AuthService) { }

    @Public()
    @Post("local/signup")
    @HttpCode(HttpStatus.CREATED)
    signupLocal(@Body() dto: AuthDto): Promise<Tokens> {
        return this.authService.signupLocal(dto);
    }

    @Public()
    @Post("local/signin")
    @HttpCode(HttpStatus.OK)
    signinLocal(@Body() dto: AuthDto): Promise<Tokens> {
        return this.authService.signinLocal(dto);
    }

    @Post("logout")
    @HttpCode(HttpStatus.OK)
    logout(@GetCurrentUserId() id: string) {
        return this.authService.logout(id);
    }

    @Public()
    @UseGuards(RtGuard)
    @Post("refresh")
    @HttpCode(HttpStatus.OK)
    refreshToken(
        @GetCurrentUser("refreshToken") rt: string,
        @GetCurrentUserId() id: string,
        @Res({ passthrough: true }) res: Response,
    ): Promise<Tokens> {
        return this.authService.refresh(id, rt, res);
    }
}

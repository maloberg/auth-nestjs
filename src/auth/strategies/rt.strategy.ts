import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import { Request } from "express";
import { BadRequestException, Injectable } from "@nestjs/common";

@Injectable()
export class RtStrategy extends PassportStrategy(Strategy, "jwt-refresh") {
    constructor() {
        super({
            jwtFromRequest: ExtractJwt.fromExtractors([ (request: Request) => {
                let rt = request?.cookies[ "rt" ];

                if (!rt) {
                    return null;
                }

                return rt;
            } ]),
            secretOrKey: "rt-secret",
            passReqToCallback: true,
        });
    }

    validate(req: Request, payload: any) {
        const refreshToken = req.cookies[ "rt" ];

        if (!refreshToken) {
            throw new BadRequestException("invalid rt");
        }

        return {
            ...payload,
            refreshToken,
        };
    }
}

import { ForbiddenException, Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";

import { Response } from "express";
import * as bcrypt from "bcrypt";

import type { Tokens, TokensPayload } from "./types";
import { PrismaService } from "src/prisma/prisma.service";
import { AuthDto } from "./dto";

@Injectable()
export class AuthService {
    constructor(
        private prisma: PrismaService,
        private jwtService: JwtService,
    ) { }

    async signupLocal(dto: AuthDto): Promise<Tokens> {
        const hash = await this.hashData(dto.password);
        const newUser = await this.prisma.user.create({
            data: {
                email: dto.email,
                password: hash,
            },
        });

        const tokens = await this.getTokens({
            email: newUser.email,
            userId: newUser.id,
        });

        this.updateRtHash(newUser.id, tokens.refresh_token);

        return tokens;
    }

    async signinLocal(dto: AuthDto): Promise<Tokens> {
        const user = await this.prisma.user.findUnique({
            where: {
                email: dto.email,
            },
        });

        if (!user) {
            throw new ForbiddenException("Acces denied");
        }

        const passwordMatches = await bcrypt.compare(
            dto.password,
            user.password,
        );

        if (!passwordMatches) {
            throw new ForbiddenException("Password Incorrect");
        }

        const tokens = await this.getTokens({
            email: user.email,
            userId: user.id,
        });

        this.updateRtHash(user.id, tokens.refresh_token);

        return tokens;
    }

    async logout(userId: string) {
        await this.prisma.user.updateMany({
            where: {
                id: userId,
                rt: {
                    not: null,
                },
            },
            data: {
                rt: null,
            },
        });
    }

    async refresh(userId: string, rt: string, res: Response) {
        const user = await this.prisma.user.findUnique({
            where: {
                id: userId,
            },
        });

        if (!user || !user.rt) {
            throw new ForbiddenException("access denied");
        }

        const rtMatches = await bcrypt.compare(rt, user.rt);

        if (!rtMatches) {
            throw new ForbiddenException("access denied");
        }

        const tokens = await this.getTokens({
            email: user.email,
            userId: user.id,
        });

        res.cookie("rt", tokens.refresh_token, {
            httpOnly: true,
        });

        this.updateRtHash(user.id, tokens.refresh_token);

        return tokens;
    }

    async updateRtHash(userId: string, rt: string) {
        const hash = await this.hashData(rt);

        await this.prisma.user.update({
            where: {
                id: userId,
            },

            data: {
                rt: hash,
            },
        });
    }

    async getTokens({ userId, email }: TokensPayload): Promise<Tokens> {
        const [ at, rt ] = await Promise.all([
            this.jwtService.signAsync(
                {
                    sub: userId,
                    email,
                },
                {
                    secret: "at-secret",
                    expiresIn: 60 * 15,
                },
            ),
            this.jwtService.signAsync(
                {
                    sub: userId,
                    email,
                },
                {
                    secret: "rt-secret",
                    expiresIn: 60 * 60 * 24 * 7,
                },
            ),
        ]);

        return {
            access_token: at,
            refresh_token: rt,
        };
    }

    hashData(data: string) {
        return bcrypt.hash(data, 10);
    }
}
